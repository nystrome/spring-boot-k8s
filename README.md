# Spring Boot on K8s

This project adds a config map that follows the approach for json logging to stdout in the elastic ECS format 1.6.
It allows for greater use with the Elastic stack and its Observability app.

There is also the cloud config watcher that refreshes the context of spring boot applications when its associated
configmap is updated on K8s. This allows for faster config changes to spring boot applicaitons running in pods.